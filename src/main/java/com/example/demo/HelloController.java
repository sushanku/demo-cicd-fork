package com.example.demo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "<h3>Hello Java Freaks!!!</h3><h2>Greetings from Spring Boot App!!!</h2>";
    }
}
